const mongoose = require('mongoose')

const taskSchema = new mongoose.Schema({
    title: String,
    description: String,
    author: String,
    completed: Boolean
})

const Tasks = mongoose.model('Tasks', taskSchema)

module.exports = {
    Tasks
}