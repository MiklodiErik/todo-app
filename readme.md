TODO APP

## Funkciók

- feladatok listázása
    - feladat szűrés
- feladat hozzáadása / szerkesztés??
    - cím
    - leírás
    - szerző
    - határidő beállítása
- feladat törlése
- megjelölés befejezettnek
    - ?? feladat státuszának megjelölése (in progress, on hold, finished)

## ENV változók beállítása

> Note: A használathoz, létre kell hozni egy .env file-t (ez szándékosan van ignorálva a .gitignore-ban, hogy minden fejlesztő saját beállításokkal tudjon tesztelni és ne kelljen mindig átírni git pull esetén)

szükséges env változók:
- MONGO_DB_CONNECTION_STRING
- PORT