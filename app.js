const express = require('express')
const bodyParser = require('body-parser')
const { v4: uuidv4 } = require('uuid')
const { TodoList } = require('./todo')
const dotenv = require('dotenv').config()
const db = require('./db-connection')
const todos = new TodoList()

const app = express()

app.use(bodyParser.json())

app.get('/welcome', (req, res) => {
    res.send("<h1>Welcome to the TODO APP!</h1>")
})

app.get('/api/tasks', (req, res) => {
    todos.listTasks()
    .then((tasks) => {
        res.status(200).json({ 
            tasks,
            count: tasks.length
        })
    })
    .catch((err) => res.status(404).json({error_message: err}))
})

app.post('/api/task', (req, res) => {
    const newTask = {
        title: req.body.title,
        description: req.body.description,
        author: req.body.author,
    }
    todos.addTask(newTask)
    .then((task) => {
        res.status(200).json({ message: `New task was added with title: '${req.body.title}'` })
    })
    .catch((err) => res.status(404).json({error_message: err}))    
})

app.delete('/api/task', (req, res) => {
    todos.removeTask(req.body.id)
    .then((response) => {
        if (response) {
            res.status(200).json({ message: `Task was deleted with ID: ${req.body.id}`})
        } else {
            res.status(404).json({ message: `ID '${req.body.id}' was not found!`})
        }
    })
    .catch((err) => res.status(404).json({error_message: err.message}))
})

app.patch('/api/toggle-completion', (req, res) => {
    todos.toggleCompletion(req.body.id)
    .then((response) => {
        if (response == true || response == false) {
            res.status(200).json({message: `completed status changed to: ${response}`})
        } else if (response == null) {
            res.status(404).json({error_message: `Id: '${req.body.id}' can not be found.`})
        }
        else {
            res.status(404).json({error_message: response.message})
        }
    })
})

app.listen(process.env.PORT, () => {
    console.log(`app is running on port ${process.env.PORT}`)
})