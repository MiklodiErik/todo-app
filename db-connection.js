const app = require('./app')
const mongoose = require('mongoose')

mongoose.connect(process.env.MONGO_DB_CONNECTION_STRING, {useNewUrlParser: true, useUnifiedTopology: true})

const db = mongoose.connection
db.on('error', (error) => console.log("Error connection to MongoDB", error))
db.once('open', () => console.log("Connected to MongoDB"))

module.exports = {
    db
}