const { Tasks } = require('./models/tasks')

class Task {
    constructor(newTask) {
        this.title = newTask.title
        this.description = newTask.description
        this.author = newTask.author
        this.completed = false
    }
}

class TodoList {

    async listTasks() {
        return await Tasks.find({})
    }
    async addTask(newTask) {
        const task = new Task(newTask)
        return await Tasks.create(task)
    }
    async removeTask(id) {
        return await Tasks.findByIdAndDelete(id)
    }
    async toggleCompletion(id) {
        try {
            const task = await Tasks.findById(id)
            if (task == null) {
                return null
            } else if (task.completed) {
                await Tasks.updateOne({_id: id}, {completed: false})
                return false
            } else if (!task.completed) {
                await Tasks.updateOne({_id: id}, {completed: true})
                return true
            }
        } catch(err) {
            return err
        }
    }
}

module.exports = { TodoList }